This is a collection of input and output files which were used to compare the GPU enabled version of Quantum ESPRESSO (QE) against the standard CPU implementation. The results are reported in the following publication:

Paolo Giannozzi, Oscar Baseggio, Pietro Bonfà, Davide Brunato, Roberto Car, Ivan Carnimeo,
Carlo Cavazzoni, Stefano de Gironcoli, Pietro Delugas, Fabrizio Ferrari Ruffino,
Andrea Ferretti, Nicola Marzari, Iurii Timrov, Andrea Urru, Stefano Baroni
"Quantum ESPRESSO at the exascale"
To appear in "Special Topic on Electronic Structure Software" of The Journal of Chemical Physics.

Calculations were performed using QE version 6.5 for CPU only runs, while QE-GPU v6.5a1 was used to produce the GPU accelerated results.
Compilation and execution details are given in the file "Details.pdf" included in this archive.


CONTENT OF FOLDERS:
 - /COD_ID/ (folder named after the COD ID of the CIF file used to generate QE input files)
    - /outputs (dump of standard output of each run)
       - cpu_Xmpi_Ypool ( results obtained without GPU acceleration. X number of MPI processes, Y value of npool option)
       - gpu_Xmpi_Ypool ( results obtained with GPU acceleration. X number of MPI processes, Y value of npool option)
    - /pseudo (folder containing pseudopotentials)
    - pwscf.in (input file, generated with Quantum ESPRESSO input generator)

 - Details.pdf (description of input files and of benchmark results)
 - Details.tex (source file of previous entry)
