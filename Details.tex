\documentclass[a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage{booktabs} % To thicken table lines
\usepackage{lscape} 
\usepackage{url} 
\title{Description of the Data}
% \author{}
\date{}
\begin{document}
\maketitle{}

The input files for all benchmarks collected in this repository have been produced with the online tool
\texttt{qeinputgenerator} \footnote{\url{https://www.materialscloud.org/work/tools/qeinputgenerator}}.

They are labeled in Tab.~\ref{tab:perf1} and Tab.~\ref{tab:perf2} according to the following syntax: 
\textsc{ID-PseudoSet-OccupationType-ReciprocalSpaceGrid}. 
\textsc{ID} is COD's structure ID, \textsc{PseudoSet} can either be ``E'' or ``P'' for SSSP Efficiency or Precision respectively. 
This choice obviously implies different values for the cutoff of the basis set. 
The \textsc{OccupationType} can be NI for ``non-magnetic insulator'' having fixed electronic band occupations, NM for ``non-magnetic metal'' 
where a smearing function (Phys. Rev. Lett. {\bf 82}, 3296 (1999)) is used for bands occupation close to the Fermi level or ``MM'' for system where spin-polarization was considered. The \textsc{ReciprocalSpaceGrid} parameter can be ``N'' for grids with 0.3 \AA$^{-1}$ distance between reciprocal space points, ``F'' for 0.2 \AA$^{-1}$ and ``V'' for 0.15 \AA$^{-1}$. In addition, in one case we have manually enabled the so-called gamma-trick and set this label to ``G''.

The simulations were performed with a single 18-core Intel(R) Xeon(R) E5-2697 v4 @ 2.30GHz (BDW) CPU 
and with the same hardware accompanied by one NVIDIA's Tesla V100 GPU card. 
The CPU results have been collected with \textsc{Quantum ESPRESSO} \ v6.5 compiled with Intel's 2019 compilers suite, 
Intel's MPI implementation and Intel's Math Kernel Library (MKL), 
while QE-GPU v6.5a1 was compiled with the PGI 19.10 Fortran compiler and linked to Intel's 2019 MKL. 
For the set of inputs detailed above, the pure MPI parallelization is the best strategy 
for the CPU version of the code, therefore we performed all simulations disabling OpenMP. 
On the other hand, the GPU enabled version requires OpenMP parallelism since the number of MPI processes is limited by the number of GPU cards installed on the system: 
each MPI process should be assigned just one accelerator \footnote{Although as a rule the code should be executed with one MPI per GPU card, over-subscription of the GPU can lead to significant speedups, especially for small test cases. Nonetheless, over-subscription by more than a factor 4 is seldom useful.}. For both the CPU and the GPU versions, only parallelism over $\mathbf{k}$-points (\textsc{-npool} option) has been used: a reasonable choice, given the relatively small dimension of the dense eigenvalue problem to be solved during iterative diagonalization.

For each benchmark we report below the values of three timers, \textsc{electrons}, \textsc{init\_run} and \textsc{forces},
extracted from the execution that provided the best time to solution
with and without GPU acceleration.

Two simulations did not converge in less than 80 iterations, the maximum value set by the automatic input generator. For this reason, their entries in Tab.~\ref{tab:perf1} and Tab.~\ref{tab:perf2} have missing values.

\begin{landscape}
	
{\small
\begin{table}

\begin{tabular}{llrrrrrrrrr}
\toprule
           Code &                                Formula &    Na &    Nk &     Ne &  SCF GPU &  SCF CPU &  Init GPU &  Init CPU &  Forces GPU &  Forces CPU \\
\midrule
 4021161-P-NI-G &                    C$_{15}$ H$_{14}$ O &  98.0 &   1.0 &  246.0 &          39.40 &          62.01 &          9.14 &          4.19 &        2.04 &        2.82 \\
 7208076-P-NI-N &                     C$_{2}$ H$_{5}$ Cl &  32.0 &  20.0 &   80.0 &          32.02 &          85.74 &         12.62 &          9.70 &        2.99 &        5.81 \\
 1000033-P-NM-F &                             CBaO$_{3}$ &  20.0 &  36.0 &  128.0 &          63.39 &         112.37 &          5.80 &          4.67 &        0.67 &        2.43 \\
 7216049-P-NI-F &              C$_{3}$ H$_{7}$ N O$_{2}$ &  52.0 &  32.0 &  144.0 &          68.15 &         198.12 &         11.00 &         12.02 &        2.29 &        5.36 \\
 7220242-E-NM-N &  C$_{14}$ H$_{18}$ Cu N$_{4}$ O$_{14}$ &  51.0 &  14.0 &  197.0 &          92.28 &         227.28 &         20.09 &         11.07 &        4.14 &        7.05 \\
 1537303-E-MM-N &                  Ba$_{2}$ Ni O$_{6}$ W &  40.0 &   4.0 &  352.0 &         136.85 &         259.73 &         10.56 &          5.50 &        1.74 &        3.31 \\
 7237523-E-NI-N &    C$_{6}$ Ba$_{2}$ Ca N$_{6}$ O$_{6}$ &  63.0 &   8.0 &  360.0 &         143.76 &         495.91 &         10.61 &         15.57 &        2.11 &        7.32 \\
 2218685-E-NI-V &            C$_{11}$ H$_{12}$ O$_{7}$ S &  62.0 &  44.0 &  208.0 &         169.84 &         537.70 &         17.87 &         24.82 &        3.40 &       11.43 \\
 1510534-P-NM-V &                       Au$_{4}$Ca$_{5}$ &  18.0 &  80.0 &  252.0 &         187.34 &         698.78 &          9.60 &         19.31 &        1.69 &       10.02 \\
 1535694-P-MM-F &                              I$_{4}$Ti &  20.0 &  31.0 &  160.0 &         239.43 &         752.44 &         12.38 &         14.70 &        2.30 &        6.13 \\
 1000110-P-NM-F &                              AlF$_{3}$ &  64.0 &  18.0 &  384.0 &         261.73 &        1088.63 &         17.55 &         43.23 &        4.59 &       28.67 \\
 2015315-E-MM-N &        Cr$_{2}$ H$_{4}$ In Na O$_{10}$ &  36.0 &  21.0 &  228.0 &         438.43 &        1335.80 &         16.83 &         16.91 &        2.94 &        9.27 \\
 4315114-E-MM-N &                    O$_{10}$ P$_{2}$ Ti &  52.0 &   9.0 &  328.0 &         690.61 &        2099.23 &         17.20 &         15.45 &           - &           - \\
 4002800-P-NM-V &                          Ca N$_{2}$ Si &  64.0 &  30.0 &  384.0 &         481.25 &        2239.83 &         26.77 &         84.98 &        6.71 &       29.76 \\
 9010914-E-MM-F &                          Fe O$_{3}$ Ti &  30.0 &  34.0 &  276.0 &        1001.73 &        2523.44 &         21.14 &         40.04 &        4.60 &       16.35 \\
 1537764-E-MM-F &                      Ni$_{7}$ Zr$_{2}$ &  36.0 &  33.0 &  600.0 &        1942.05 &        8267.06 &         16.81 &         43.44 &           - &           - \\
\bottomrule
\end{tabular}\caption{Comparison of timers describing self consistency, initialization and estimation of atomic forces as obtained from \textsc{Quantum ESPRESSO} v6.5 and from QE-GPU v6.5a1.
The first column describes each input according to the syntax detailed in the main text. The second, third, fourth and fifth columns are, in order, the chemical formula, the number of atoms
in the simulation cell, the number of points used to sample the reciprocal space and the number of electrons considered in the simulation.
The last six columns report the values of timers \textsc{electrons}, \textsc{init\_run} and \textsc{forces} for the simulations that provided the best time to solution for the CPU/GPU version of \textsc{pw.x}.
\label{tab:perf1}}
\end{table}

\begin{table}
\begin{tabular}{llrrrrrrr}
\toprule
           Code &                                Formula &    Na &    Nk &     Ne &       E (Ry) GPU &        E (Ry) CPU &     Total F (Ry/a.u.) GPU  &   Total F (Ry/a.u.) CPU \\
\midrule
 4021161-P-NI-G &                    C$_{15}$ H$_{14}$ O &  98.0 &   1.0 &  246.0 &  -986.409166 &  -986.409166 &  2.659261 &  2.659265  \\
 7208076-P-NI-N &                     C$_{2}$ H$_{5}$ Cl &  32.0 &  20.0 &   80.0 &  -308.879317 &  -308.879317 &  1.899620 &  1.899619  \\
 1000033-P-NM-F &                             CBaO$_{3}$ &  20.0 &  36.0 &  128.0 & -2320.098188 & -2320.098188 &  0.108992 &  0.108969  \\
 7216049-P-NI-F &              C$_{3}$ H$_{7}$ N O$_{2}$ &  52.0 &  32.0 &  144.0 &  -670.149218 &  -670.149218 &  1.767410 &  1.767409  \\
 7220242-E-NM-N &  C$_{14}$ H$_{18}$ Cu N$_{4}$ O$_{14}$ &  51.0 &  14.0 &  197.0 & -1345.239207 & -1345.239207 &  3.624060 &  3.624046  \\
 1537303-E-MM-N &                  Ba$_{2}$ Ni O$_{6}$ W &  40.0 &   4.0 &  352.0 & -6502.051439 & -6502.051439 &  0.691446 &  0.691441  \\
 7237523-E-NI-N &    C$_{6}$ Ba$_{2}$ Ca N$_{6}$ O$_{6}$ &  63.0 &   8.0 &  360.0 & -4290.504065 & -4290.504065 &  0.096581 &  0.096589  \\
 2218685-E-NI-V &            C$_{11}$ H$_{12}$ O$_{7}$ S &  62.0 &  44.0 &  208.0 & -1064.616141 & -1064.616141 &  2.045636 &  2.045636  \\
 1510534-P-NM-V &                       Au$_{4}$Ca$_{5}$ &  18.0 &  80.0 &  252.0 & -2947.640068 & -2947.640068 &  0.017008 &  0.017008  \\
 1535694-P-MM-F &                              I$_{4}$Ti &  20.0 &  31.0 &  160.0 & -6540.643580 & -6540.643580 &  0.036992 &  0.036980  \\
 1000110-P-NM-F &                              AlF$_{3}$ &  64.0 &  18.0 &  384.0 & -3007.526878 & -3007.526878 &  0.121087 &  0.121087  \\
 2015315-E-MM-N &        Cr$_{2}$ H$_{4}$ In Na O$_{10}$ &  36.0 &  21.0 &  228.0 & -2025.901395 & -2025.901395 &  1.053888 &  1.053889  \\
 4315114-E-MM-N &                    O$_{10}$ P$_{2}$ Ti &  52.0 &   9.0 &  328.0 &            - &            - &         - &         -  \\
 4002800-P-NM-V &                          Ca N$_{2}$ Si &  64.0 &  30.0 &  384.0 & -2051.267572 & -2051.267572 &  0.008444 &  0.008444  \\
 9010914-E-MM-F &                          Fe O$_{3}$ Ti &  30.0 &  34.0 &  276.0 & -3443.403951 & -3443.403951 &  0.137640 &  0.137630  \\
 1537764-E-MM-F &                      Ni$_{7}$ Zr$_{2}$ &  36.0 &  33.0 &  600.0 &            - &            - &         - &         -  \\
\bottomrule
\end{tabular}\caption{\label{tab:perf2} Comparison of total energies (columns 6 and 7) and total forces (columns 8 and 9) obtained with \textsc{Quantum ESPRESSO} v6.5 and QE-GPU v6.5a1. The meaning of the other columns is the same as in the previous table.}
\end{table}
}
\end{landscape}



\end{document}

